<?php

namespace Drupal\google_translate\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'GoogleTranslateBlock' block.
 *
 * @Block(
 *  id = "default_gtblock",
 *  admin_label = @Translation("Google Translate"),
 * )
 */
class GoogleTranslateBlock extends BlockBase
{
  protected $languages = [
    "en" => "English",
    "ar" => "Arabic",
    "bg" => "Bulgarian",
    "zh-CN" => "Chinese (Simplified)",
    "zh-TW" => "Chinese (Traditional)",
    "hr" => "Croatian",
    "cs" => "Czech",
    "da" => "Danish",
    "nl" => "Dutch",
    "fi" => "Finnish",
    "fr" => "French",
    "de" => "German",
    "el" => "Greek",
    "hi" => "Hindi",
    "it" => "Italian",
    "ja" => "Japanese",
    "ko" => "Korean",
    "no" => "Norwegian",
    "pl" => "Polish",
    "pt" => "Portuguese",
    "ro" => "Romanian",
    "ru" => "Russian",
    "es" => "Spanish",
    "sv" => "Swedish",
    "ca" => "Catalan",
    "tl" => "Filipino",
    "iw" => "Hebrew",
    "id" => "Indonesian",
    "lv" => "Latvian",
    "lt" => "Lithuanian",
    "sr" => "Serbian",
    "sk" => "Slovak",
    "sl" => "Slovenian",
    "uk" => "Ukrainian",
    "vi" => "Vietnamese",
    "sq" => "Albanian",
    "et" => "Estonian",
    "gl" => "Galician",
    "hu" => "Hungarian",
    "mt" => "Maltese",
    "th" => "Thai",
    "tr" => "Turkish",
    "fa" => "Persian",
    "af" => "Afrikaans",
    "ms" => "Malay",
    "sw" => "Swahili",
    "ga" => "Irish",
    "cy" => "Welsh",
    "be" => "Belarusian",
    "is" => "Icelandic",
    "mk" => "Macedonian",
    "yi" => "Yiddish",
    "hy" => "Armenian",
    "az" => "Azerbaijani",
    "eu" => "Basque",
    "ka" => "Georgian",
    "ht" => "Haitian Creole",
    "ur" => "Urdu",
    "bn" => "Bengali",
    "bs" => "Bosnian",
    "ceb" => "Cebuano",
    "eo" => "Esperanto",
    "gu" => "Gujarati",
    "ha" => "Hausa",
    "hmn" => "Hmong",
    "ig" => "Igbo",
    "jw" => "Javanese",
    "kn" => "Kannada",
    "km" => "Khmer",
    "lo" => "Lao",
    "la" => "Latin",
    "mi" => "Maori",
    "mr" => "Marathi",
    "mn" => "Mongolian",
    "ne" => "Nepali",
    "pa" => "Punjabi",
    "so" => "Somali",
    "ta" => "Tamil",
    "te" => "Telugu",
    "yo" => "Yoruba",
    "zu" => "Zulu",
    "my" => "Myanmar (Burmese)",
    "ny" => "Chichewa",
    "kk" => "Kazakh",
    "mg" => "Malagasy",
    "ml" => "Malayalam",
    "si" => "Sinhala",
    "st" => "Sesotho",
    "su" => "Sudanese",
    "tg" => "Tajik",
    "uz" => "Uzbek",
    "am" => "Amharic",
    "co" => "Corsican",
    "haw" => "Hawaiian",
    "ku" => "Kurdish (Kurmanji)",
    "ky" => "Kyrgyz",
    "lb" => "Luxembourgish",
    "ps" => "Pashto",
    "sm" => "Samoan",
    "gd" => "Scottish Gaelic",
    "sn" => "Shona",
    "sd" => "Sindhi",
    "fy" => "Frisian",
    "xh" => "Xhosa",
  ];
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration()
  {
    return [
      "widget_type" => "external",
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state)
  {
    $form["widget_type"] = [
      "#type" => "select",
      "#title" => $this->t("Widget Type"),
      "#description" => $this->t("Google Translate Widget Settings"),
      "#options" => [
        "inline" => $this->t("inline"),
        "external" => $this->t("external"),
      ],
      "#default_value" => $this->configuration["widget_type"],
      "#weight" => "0",
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state)
  {
    $this->configuration["widget_type"] = $form_state->getValue("widget_type");
  }

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $build = [];
    if ($this->configuration["widget_type"] === "inline") {
      $build["#theme"] = "default_gtblock";
    } else {
      $build["#theme"] = "external_gtblock";
      $build["#content"]["translate_to"] = [
        "#type" => "select",
        "#title" => $this->t("Translate to"),
        //'#default_value' =>
        "#size" => 1,
        "#options" => $this->languages,
        "#required" => true,
      ];
    }

    // $build["#content"][] = $this->configuration["widget_type"];

    return $build;
  }
}
