# Description

The module provides a block that provides the translation service from Google.

Two types of widget:  
- Inline translation that gives user to translate the content on the website without using translate.google.com website
- External translation that will lead user to a translated page via translate.google.com service.
