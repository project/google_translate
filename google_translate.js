function googleTranslateElementInit() {
  Drupal.behaviors.google_translate.attachInline(jQuery("document"));
}

(function($, Drupal, drupalSettings) {
  Drupal.behaviors.google_translate = {
    attachInline: function(context) {
      $("#google_translate_element")
        .once("it")
        .each(function() {
          const dynid = "google_translate_element" + Date.now();
          $(this).attr("id", dynid);
          new google.translate.TranslateElement({ pageLanguage: "en" }, dynid);
        });
    },
    attach: function(context) {
      $(".block-google-translate .form-select", context)
        .once("it")
        .change(function() {
          const cururl = window.location.href;
          const lang = $(this).val();
          const url = `http://translate.google.com/translate?u=${cururl}&sl=en&tl=${lang}&hl=en&ie=UTF-8`;
          window.location.href = url;
        });
    },
  };
})(jQuery, Drupal, drupalSettings);
